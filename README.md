# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
Le site [JVC](https://www.jeuxvideo.com/) est mieux adapté aux écrans larges.
## Prévisualisation
![Preview-home-page](https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-home.jpg)

## Autres prévisualisation
- https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-game.jpg
- https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-game-forum.jpg
- https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-game-news.jpg
- https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-game-tests.jpg
- https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-game-videos.jpg
- https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-game-wikis.jpg
- https://gitlab.com/breatfr/jvc/-/raw/main/docs/preview-news.jpg

## Liste des personnalisations disponibles
- mode écran larges
- pas de choses inutiles (vidéo d'intro, etc)
- taille de texte personnalisée

## Comment l'utiliser en quelques étapes
1. Installer l'extension de navigateur Stylus
    - Lien pour les navigateurs basés sur Chromium : https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - et bien d'autres
    - Lien pour les navigateurs basés sur Firefox : https://addons.mozilla.org/firefox/addon/styl-us/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - et bien d'autres

2. Aller surle site  [UserStyles.world](https://userstyles.world/style/16605) et cliquez sur `Install` en dessous de la capture d'écran ou ouvrir la [version GitLab](https://gitlab.com/breatfr/jvc/-/raw/main/css/jvc-responsive-personnalisations.user.css).

3. Pour mettre à jour le thème, ouvrez la fenêtre `Gestion des Styles` et cliquez sur `Vérifier la mise à jour` et suivez les instructions ou attendez simplement 24h pour la mise à jour automatique.

4. Profitez-en :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>